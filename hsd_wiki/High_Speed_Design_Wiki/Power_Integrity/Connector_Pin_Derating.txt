Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.6
Creation-Date: 2021-03-14T16:22:22-04:00

====== Connector Pin Derating ======

Total current through connectors must be derated when using more than one pair of pins. If the manufacturer does not provide the information, the following rule of thumb may be used:

* I = Current rating for a single pair of pins (Amps)
* N = Total number of pairs of pins used (so, N=2 for 2 power/2 ground)
* D = Derated current (Amps) for all pins used

{{./equation.png?type=equation}}

Essentially, you typically derate 20% (depends on the connector) for every doubling of pins. So 3A for one pin pair (in/out) bus, but 4.8A for two pairs (3*2*.8) and 7.68A for 4 pairs (3*4*.8*.8).

(Special thanks to Paul Davis from LGS Innovations/CACI for sharing this with me when I was just starting out)

