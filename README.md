This repo holds all of the base files and html outputs for the High Speed Design Wiki located at https://wiki.shielddigitaldesign.com. 

The wiki is created in Zim (https://zim-wiki.org/). The Zim notebook is in the hsd_wiki directory.

The template directory contains the HTML template used for export.

The html directory holds all of the files which are uploaded to the web server.

The custom command used to export the Zim notebook to HTML is:

zim --export -O --output=C:\Users\steph\Documents\ShieldDigitalDesign\Administrative\Website\sdd\wiki\html --format=html --template=C:\Users\steph\Documents\ShieldDigitalDesign\Administrative\Website\sdd\wiki\template\SDD_Template.html --index-page=index C:\Users\steph\Documents\ShieldDigitalDesign\Administrative\Website\sdd\wiki\hsd_wiki\notebook.zim
