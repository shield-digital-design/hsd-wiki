import tempfile
import sys
import os

def escape_html():

    directory = './html' # Run before renaming directory

    # Checks all files in directory
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if filename[-4:] == "html": # Only on HTML files
                fin = open(os.path.join(root, filename), 'r')
                t = tempfile.NamedTemporaryFile(mode="r+")
                replace_flag = 0
                for line in fin:
                    if "start_html" in line:
                        replace_flag = 1
                        print("HTML flag on")
                    elif "end_html" in line:
                        replace_flag = 0
                        print("HTML flag off")
                    else:
                        if replace_flag == 1:
                            line = line.replace('&amp;', '&')
                            line = line.replace('&lt;', '<')
                            line = line.replace('&gt;', '>')
                        t.write(line)
                fin.close()
                t.seek(0)
                o = open(os.path.join(root, filename), "w")
                for line in t:
                    o.write(line)
                t.close()    
                o.close()
                print(f"Completed File ---- {filename}")
            else:
                print(f"File not HTML ----- {filename}")    
            
if __name__ == "__main__":
    escape_html()
